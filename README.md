# 代码生成器使用文档

| 文档创建日期     | 版本号  | 文档说明           | 创建人 |
|:----------:|:----:|:--------------:|:---:|
| 2018/06/14 | V1.0 | 代码生成器网页版使用方法说明 | 如漩涡 |

## 介绍

> 代码生成器是一个基于Spring Boot & MyBatis的种子项目，用于快速构建中小型API、RESTful API项目，该种子项目已经有过多个真实项目的实践，稳定、简单、快速，使我们摆脱那些重复劳动，专注于业务代码的编写，减少加班。

代码生成器网页版是分项目core层、app层、独立项目的模版进行生成，项目架构清晰统一。在界面中连接数据库，可以查看当前数据库中有多少张表，选中某一张表可查看这张表的所有字段、字段类型、字段注释；在代码生成功能模块中，填写相应的信息，点击生成即可下载对应的项目，解压可直接使用。使用代码分离的结构，将Service层、Controller层分为code层，app层，也可以生成一个完成的项目，这个工具的好处是：生成的代码风格统一，助于公司规范。

## 特征

* 最佳实践的项目结构、配置文件、精简的POM

* 统一响应结果封装及生成工具 

* 统一异常处理

* 常用基础方法抽象封装

* 使用Druid Spring Boot Starter 集成Druid数据库连接池与监控

* 使用FastJsonHttpMessageConverter，提高JSON序列化速度

* 集成MyBatis、通用Mapper插件、PageHelper分页插件，实现单表业务零SQL

* 提供代码生成器根据表名生成对应的模块，减少重复代码的编写，助于保持团队代码风格的统一 

## 使用步骤

**1. 启动项目**

下载项目运行到IDEA中，修改`resources`目录下的配置文件**code-config.properties**，其中有一个选项`linux.file.path`，本地启动的话，选择一个盘符，linux坏境部署的话，改为home/；同在`resources`目录下找到static目录的config目录，修改`ip.json`里的`ip:`，端口号与配置文件`application.properties`的端口号一致，地址可改为本地的，部署到服务器就改成服务器地址；

以上的修改完毕，启动`Application`类即可

**2. 访问页面**

地址：http://localhost:8087/

![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/1.png)





**3. 连接数据库**

* 页面中有两个功能模块，必须先连接数据库，不然代码生成功能模块无法使用

* 选择数据库类型，目前支持数据库仅mysql，但技术已经实现各主流数据库的连接以及生成，如需扩展，会更新版本

* 填写数据库IP、用户名、密码以及数据库名，点击测试连接，连接失败会返回相应错误信息提醒

* 连接成功会跳转到查看表的界面

  ![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/2.png)

  点击选中某一张表，会显示当前表的信息，方便开发人员查看信息，无需在开发中忘记这张表字段、字段类型、字段注释还要打开数据库工具查看

  ![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/3.png)

  

**4. 代码生成**

* 数据库连接成功后，点击代码生成功能模块

  ![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/4.png)

* 填写创建人(author)、项目名、包路径名称和要创建的项目类型以及GroupId，缺一不可

  1. 创建人是便于知道当前项目由谁负责 

  2. 项目名是要定义项目的名称 

  3. 包名更是一个项目不能缺少的

  4. GroupId更方便自定义pom

  5.  项目类型便于要为开发人员生成core层、app层、独立项目和代码片段的代码 

     * core层说明

       存放实体类、DTO、mapper接口以及xml、Service以及实现类

     * app层说明

       存放配置、统一异常、过滤器、Controller；这种架构是为了代码分离，需要实现业务逻辑在core层实现，打包jar，给app层pom文件引入，便于维护

     * 独立项目说明

       合并core层和app层的一个完整项目

     * 代码片段说明

       比方说在网页上生成了一个项目，之后有了新的需求要新增几张表的时候，不需要重复生成项目，项目类型选择代码片段，加入要新增的几张表，点击下载后生成的zip是一个分层好的各个文件(Service, ServiceImpl, Domain, DTO, Mapper, MapperXML, Controller)供方便复制到原来的项目中

* 点击`+`的按钮，多表一起生成的话多点击几次

  ![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/5.png)

  

* 在表名一栏的下拉框中选择要生成的表，在主键类型一栏的下拉框中选择是UUID还是INT自增，便于“insert”ID回显描述一栏会在选择表名的时候自动根据表注释填充，没有注释的话开发人员请手动填写当前表的描述

* 填写完之后，点击代码生成按钮，会在后台自动生成代码并且压缩成包，以下载的形式导出

  ![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/7.png)

**5. 运行项目**

将下载的压缩包放入磁盘中解压，运行在IDEA里；项目中需要的依赖包，如果setting文件配置正确，会自动去下载

![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/8.png)

**生成的项目无需做修改配置，除非有需求；pom文件中自动导入了需要的依赖包，两个application.properties配置文件自动配置完成，数据库配置也一起生成**

![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/9.png)

直接Debug启动类运行项目

![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/10.png)



接口测试：

![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/11.png)



## 其他

之前的开发中，代码生成器只对INT自增的ID有插入数据库后ID回显，UUID的开发人员为此不得不在每个生成的实体类id这个属性注解上修改，很耗时长，也磨灭耐心；在代码生成功能模块中，让开发人员选择主键类型，UUID和INT自增，用于回显的，UUID开发人员生成的实体类上会自动加上回显的代码，无需手动修改

1. UUID效果

   ![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/12.png)

2. 自增效果

   ![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/13.png)



另外生成的实体类自带“hashCode”、“equals”、“toString”方法

![](https://gitee.com/ruxuanwo/imgRepository/raw/master/generator-img/14.png)



**注意：**下载core层代码，放入IDEA工具确认无误，先执行Maven命令本地安装也可以在IDEA自带的Maven插件中顺序执行本地安装；Maven命令：`mvn clean install`

打包安装到本地后，再下载app层代码，运行在IDEA中，在pom文件中手动添加core层依赖，避免Controller层的Service注入报错，app层项目名称不要和core相同；

1. core层命名实例：core-demo

2. app层命名实例：app-demo 
