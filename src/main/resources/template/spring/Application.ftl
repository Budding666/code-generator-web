package ${basePackage};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * 启动类
 * @author ${author} on ${date}
 */
@SpringBootApplication
@ComponentScan(basePackages={"${basePackage}"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}