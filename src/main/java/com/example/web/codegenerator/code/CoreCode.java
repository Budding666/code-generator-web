package com.example.web.codegenerator.code;

import com.example.web.codegenerator.config.CodeConfig;
import com.example.web.codegenerator.exception.GeneratorException;
import com.example.web.codegenerator.exception.SqlErrorException;
import com.example.web.codegenerator.model.DbConfig;
import com.example.web.codegenerator.model.Model;
import com.example.web.codegenerator.model.PathConfig;
import com.example.web.codegenerator.model.ProjectConfig;
import com.example.web.codegenerator.util.CodeUtil;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.*;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoreCode {


    public static void getDomainAndMapper(Model model, DbConfig dbConfig, ProjectConfig projectConfig, String fileName, String type) {
        Context context = new Context(ModelType.FLAT);
        context.setId(CodeConfig.CONTEXT_ID);
        context.setTargetRuntime(CodeConfig.TARGET_RUNTIME);
        context.addProperty(PropertyRegistry.CONTEXT_BEGINNING_DELIMITER, "`");
        context.addProperty(PropertyRegistry.CONTEXT_ENDING_DELIMITER, "`");

        String connectionURL = "jdbc:" + dbConfig.getDbType() + "://" + dbConfig.getUrl() + ":"
                + dbConfig.getPort() + "/" + dbConfig.getDataBase()
                + CodeConfig.CONFIG_URL;
        JDBCConnectionConfiguration jdbcConnectionConfiguration = new JDBCConnectionConfiguration();
        jdbcConnectionConfiguration.setConnectionURL(connectionURL);
        jdbcConnectionConfiguration.setUserId(dbConfig.getUser());
        jdbcConnectionConfiguration.setPassword(dbConfig.getPassWord());
        jdbcConnectionConfiguration.setDriverClass(dbConfig.getDriverClass());
        context.setJdbcConnectionConfiguration(jdbcConnectionConfiguration);

        PluginConfiguration pluginConfiguration = new PluginConfiguration();
        pluginConfiguration.setConfigurationType("tk.mybatis.mapper.generator.MapperPlugin");
        pluginConfiguration.addProperty("mappers", projectConfig.getMAPPER_INTERFACE_REFERENCE());
        context.addPluginConfiguration(pluginConfiguration);

        // Domain实现序列化接口
        PluginConfiguration serializablePlugin = new PluginConfiguration();
        serializablePlugin.setConfigurationType("org.mybatis.generator.plugins.SerializablePlugin");
        serializablePlugin.addProperty("domain", PathConfig.domain);
        context.addPluginConfiguration(serializablePlugin);

        //Domain实现equals、hashCode生成
        PluginConfiguration equalsHashCodePlugin = new PluginConfiguration();
        equalsHashCodePlugin.addProperty("type", "org.mybatis.generator.plugins.EqualsHashCodePlugin");
        equalsHashCodePlugin.setConfigurationType("org.mybatis.generator.plugins.EqualsHashCodePlugin");
        context.addPluginConfiguration(equalsHashCodePlugin);

        //Domain实现toString生成
        PluginConfiguration toStringPlugin = new PluginConfiguration();
        toStringPlugin.addProperty("type", "org.mybatis.generator.plugins.ToStringPlugin");
        toStringPlugin.setConfigurationType("org.mybatis.generator.plugins.ToStringPlugin");
        context.addPluginConfiguration(toStringPlugin);

        JavaModelGeneratorConfiguration javaModelGeneratorConfiguration = new JavaModelGeneratorConfiguration();

        SqlMapGeneratorConfiguration sqlMapGeneratorConfiguration = new SqlMapGeneratorConfiguration();

        JavaClientGeneratorConfiguration javaClientGeneratorConfiguration = new JavaClientGeneratorConfiguration();
        javaClientGeneratorConfiguration.setConfigurationType(CodeConfig.CONFIGURATION_TYPE);

        if ("4".equals(type)){
            javaModelGeneratorConfiguration.setTargetProject(fileName);
            javaModelGeneratorConfiguration.setTargetPackage(projectConfig.getDomain());
            sqlMapGeneratorConfiguration.setTargetProject(fileName);
            sqlMapGeneratorConfiguration.setTargetPackage(projectConfig.getPackAge() + "/mapper/xml");
            javaClientGeneratorConfiguration.setTargetProject(fileName);
            javaClientGeneratorConfiguration.setTargetPackage(projectConfig.getMapper());
        }else {
            javaModelGeneratorConfiguration.setTargetProject(fileName + PathConfig.JAVA_PATH);
            javaModelGeneratorConfiguration.setTargetPackage(projectConfig.getDomain());
            sqlMapGeneratorConfiguration.setTargetProject(fileName + PathConfig.RESOURCES_PATH);
            sqlMapGeneratorConfiguration.setTargetPackage("mybatis/mapper");
            javaClientGeneratorConfiguration.setTargetProject(fileName + PathConfig.JAVA_PATH);
            javaClientGeneratorConfiguration.setTargetPackage(projectConfig.getMapper());
        }
        context.setJavaModelGeneratorConfiguration(javaModelGeneratorConfiguration);
        context.setSqlMapGeneratorConfiguration(sqlMapGeneratorConfiguration);
        context.setJavaClientGeneratorConfiguration(javaClientGeneratorConfiguration);

        TableConfiguration tableConfiguration = new TableConfiguration(context);
        tableConfiguration.setTableName(model.getTableName());
        if (StringUtils.isNotEmpty(model.getDomainName())) {
            tableConfiguration.setDomainObjectName(model.getDomainName());
        }
        String uuid = "String";
        if (model.getPkDataType().equals(uuid)) {
            tableConfiguration.setGeneratedKey(new GeneratedKey("id", "SELECT REPLACE(UUID(),\\\"-\\\",\\\"\\\")", false, "pre"));
        }else {
            tableConfiguration.setGeneratedKey(new GeneratedKey("id", "Mysql", true, null));
        }
        context.addTableConfiguration(tableConfiguration);

        List<String> warnings;
        MyBatisGenerator generator;

        String usingPassword = "using password";
        String unknownDatabase = "Unknown database";
        String linkFailure = "link failure";
        try {
            Configuration config = new Configuration();
            config.addContext(context);
            config.validate();

            boolean overwrite = true;
            DefaultShellCallback callback = new DefaultShellCallback(overwrite);
            warnings = new ArrayList<String>();
            generator = new MyBatisGenerator(config, callback, warnings);
            generator.generate(null);
        } catch (Exception e) {
            if (e.getMessage().contains(usingPassword)) {
                throw new SqlErrorException("数据库用户名或密码错误！");
            }
            if (e.getMessage().contains(unknownDatabase)) {
                throw new SqlErrorException("找不到数据库名" + e.getMessage().substring((e.getMessage().indexOf("'")) + 1, e.getMessage().lastIndexOf("'")));
            }
            if (e.getMessage().contains(linkFailure)) {
                throw new SqlErrorException("数据库连接失败，请检查IP和端口是否正确！");
            }
            throw new GeneratorException("数据库连接失败，请检查配置是否正确" + e);
        }

        if (generator.getGeneratedJavaFiles().isEmpty() || generator.getGeneratedXmlFiles().isEmpty()) {
            throw new GeneratorException("生成Domain和Mapper失败：" + warnings);
        }

        String domainName = CodeUtil.tableNameConvertUpperCamel(model.getTableName());
        System.out.println(domainName + ".java 生成成功");
        System.out.println(domainName + "Mapper.java 生成成功");
        System.out.println(domainName + "Mapper.xml 生成成功");
    }

    public static void getService(Model model, String author,ProjectConfig projectConfig, String fileName, String type) {
        Map<String, Object> data = CodeUtil.setParam(model, author, projectConfig);
        String savePath, savePath1, tempName, tempName1;
        if ("4".equals(type)){
            savePath = fileName + PathConfig.service + data.get("domainNameUpperCamel") + "Service.java";
            tempName = "code/service.ftl";

            savePath1 = fileName + PathConfig.serviceImpl + data.get("domainNameUpperCamel") + "ServiceImpl.java";
            tempName1 = "code/service-impl.ftl";
        }else {
            savePath = fileName + PathConfig.JAVA_PATH + PathConfig.service + data.get("domainNameUpperCamel") + "Service.java";
            tempName = "code/service.ftl";

            savePath1 = fileName + PathConfig.JAVA_PATH + PathConfig.serviceImpl + data.get("domainNameUpperCamel") + "ServiceImpl.java";
            tempName1 = "code/service-impl.ftl";
        }

        try {
            CodeUtil.tempFactory(savePath, tempName, data);
            System.out.println(data.get("domainNameUpperCamel") + "Service.java 生成成功");

            CodeUtil.tempFactory(savePath1, tempName1, data);
            System.out.println(data.get("domainNameUpperCamel") + "ServiceImpl.java 生成成功");
        } catch (Exception e) {
            throw new GeneratorException("生成Service失败");
        }

    }

    public static void getDTO(Model model, String author, ProjectConfig projectConfig, String fileName, String type) {
        try {
            Map<String, Object> data = CodeUtil.setParam(model, author,projectConfig);
            String savePath, tempName;
            if ("4".equals(type)){
                savePath = fileName + PathConfig.dto + data.get("domainNameUpperCamel") + "DTO.java";
                tempName = "code/dto.ftl";
            }else {
                savePath = fileName + PathConfig.JAVA_PATH + PathConfig.dto + data.get("domainNameUpperCamel") + "DTO.java";
                tempName = "code/dto.ftl";
            }
            CodeUtil.tempFactory(savePath, tempName, data);
            System.out.println(data.get("domainNameUpperCamel") + "DTO.java 生成成功");
        } catch (Exception e) {
            throw new GeneratorException("生成DTO失败");
        }
    }

    public static void getMapper(ProjectConfig projectConfig, String fileName){
        try {
            Map<String, Object> data = new HashMap<>(16);
            data.put("basePackage", projectConfig.getPackAge());

            String savePath = fileName + PathConfig.JAVA_PATH + PathConfig.core + "Mapper.java";
            String tempName = "code/Mapper.ftl";

            CodeUtil.tempFactory(savePath, tempName, data);
            System.out.println("Mapper.java 生成成功");
        } catch (Exception e) {
            throw new GeneratorException("生成Mapper失败");
        }
    }
    public static void getOrderBy(ProjectConfig projectConfig, String fileName){
        try {
            Map<String, Object> data = new HashMap<>(16);
            data.put("basePackage", projectConfig.getPackAge());

            String savePath = fileName + PathConfig.JAVA_PATH + PathConfig.core + "OrderBy.java";
            String tempName = "code/OrderBy.ftl";

            CodeUtil.tempFactory(savePath, tempName, data);
            System.out.println("OrderBy.java 生成成功");
        } catch (Exception e) {
            throw new GeneratorException("生成OrderBy失败");
        }
    }
    public static void getQueryService(ProjectConfig projectConfig, String fileName){
        try {
            Map<String, Object> data = new HashMap<>(16);
            data.put("basePackage", projectConfig.getPackAge());

            String savePath = fileName + PathConfig.JAVA_PATH + PathConfig.core + "Service.java";
            String tempName = "code/QueryService.ftl";

            CodeUtil.tempFactory(savePath, tempName, data);
            System.out.println("Service.java 生成成功");
        } catch (Exception e) {
            throw new GeneratorException("生成Service失败");
        }
    }
    public static void getAbService(ProjectConfig projectConfig, String fileName){
        try {
            Map<String, Object> data = new HashMap<>(16);
            data.put("basePackage", projectConfig.getPackAge());

            String savePath = fileName + PathConfig.JAVA_PATH + PathConfig.core + "AbstractService.java";
            String tempName = "code/AbstractService.ftl";

            CodeUtil.tempFactory(savePath, tempName, data);
            System.out.println("AbstractService.java 生成成功");
        } catch (Exception e) {
            throw new GeneratorException("生成AbstractService失败");
        }
    }
}
