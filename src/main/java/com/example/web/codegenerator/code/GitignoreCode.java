package com.example.web.codegenerator.code;

import com.example.web.codegenerator.exception.GeneratorException;
import com.example.web.codegenerator.util.CodeUtil;

public class GitignoreCode {

    public static void getGitignore(String fileName) {
        try {
            String savePath = fileName + "/.gitignore";
            String tempName = "config/gitignore.ftl";
            CodeUtil.tempFactory(savePath, tempName, null);
            System.out.println("gitignore 生成成功");
        }catch (Exception e){
            throw new GeneratorException("生成gitignore失败");
        }
    }
}
