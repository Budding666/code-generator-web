package com.example.web.codegenerator.generator.impl;

import com.example.web.codegenerator.generator.Generator;
import com.example.web.codegenerator.code.CoreCode;
import com.example.web.codegenerator.code.WebCode;
import com.example.web.codegenerator.model.DbConfig;
import com.example.web.codegenerator.model.Model;
import com.example.web.codegenerator.model.ProjectConfig;

import java.io.File;

public class PartGenerator implements Generator {
    @Override
    public void makeCode(Model model, String author, ProjectConfig projectConfig, DbConfig dbConfig, String fileName, String type, String groupId) {
        File file = new File(fileName);
        if (!file.exists()){
            file.mkdirs();
        }
        CoreCode.getDomainAndMapper(model, dbConfig, projectConfig, fileName, type);
        CoreCode.getService(model, author, projectConfig, fileName, type);
        CoreCode.getDTO(model, author, projectConfig, fileName, type);
        WebCode.getController(model, author, projectConfig, fileName, type);
    }
}
