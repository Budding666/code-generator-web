package com.example.web.codegenerator.generator.impl;

import com.example.web.codegenerator.generator.Generator;
import com.example.web.codegenerator.code.GitignoreCode;
import com.example.web.codegenerator.code.PomCode;
import com.example.web.codegenerator.code.WebCode;
import com.example.web.codegenerator.model.DbConfig;
import com.example.web.codegenerator.model.Model;
import com.example.web.codegenerator.model.ProjectConfig;
import com.example.web.codegenerator.util.CodeUtil;

public class AppGenerator implements Generator {
    @Override
    public void makeCode(Model model, String author, ProjectConfig projectConfig, DbConfig dbConfig, String fileName, String type, String groupId) {
        CodeUtil.setLinuxFile(fileName);
        WebCode.getController(model, author, projectConfig, fileName, type);
        PomCode.getPom(fileName, type, groupId);
        WebCode.getLogBack(projectConfig, fileName);
        WebCode.getMybatisConfig(model, author, projectConfig, fileName);
        WebCode.getServiceFilter(author, projectConfig, fileName);
        WebCode.getWebMvcConfig(model, author, projectConfig, fileName);
        WebCode.getGlobalExceptionHandler(model, author, projectConfig, fileName);
        WebCode.getApplication(model, author, projectConfig, fileName);
        WebCode.getApplicationDev(fileName, dbConfig);
        WebCode.getApplicationProperties(fileName);
        WebCode.getResult(model, author, fileName, projectConfig);
        WebCode.getResponseMsgUtil(model, author, fileName, projectConfig);
        WebCode.getRequestUtil(model, author, fileName, projectConfig);
        GitignoreCode.getGitignore(fileName);
    }
}
